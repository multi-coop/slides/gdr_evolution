---
title: Revues d'itération GDR-Santé
subtitle: 
author: multi.coop
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo_client.svg"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# 26 avril 2024 - Soumission de la Page Dossier !

## Ordre du jour 

- météo 
- démo
- points sur les priorités
- questions/clarifications
- prochaine revue - le 17 mai

## Démo

![Page dossier](images/240426/demo.png)

## Points sur les priorités

---

### Objectifs et métriques clefs

Point sur les [Okr](https://github.com/orgs/dataoffice-ansm/projects/1/views/8) 

 

:::::::::::::: {.columns}
::: {.column width="50%"}
- Définir clairement nos Okr
- Rattacher chaque tache épique à l'Okr associée
- Réfléchir à des façons de mesurer l'impact de la métrique
::: 

::: {.column width="50%"}
<img src="images/240426/atelier.jpg" alt="Atelier métriques" height="500px">
:::
::::::::::::::

---

### Nouvelles taches épiques

 Consulter les critères métiers pertinents - cf [tache 220](https://github.com/dataoffice-ansm/gdr_sante/issues/220)

<img src="images/240426/metier.png" alt="Critères concernés" height="500px"> 

## Questions à explorer

- @Mauko, une suite à l'atelier métrique ?

- Gestion des droits d'éditions pour l'évaluation des risques
    - qui a les droits ? une/deux personnes pour chaque soumissions ? tous les agent de la DA ?
    - Quel service est responsable de l'attribution des autorisations ? (piste : lorsqu'un utilisateur est redirigé depuis STD, STD lui fournit un token d'autorisation ? gestion des rôles internes à GDR ?)
    - A t'on besoin d'enregistrer l'auteur des modifications ? ou simplement vérifier que l'auteur a les droits nécessaire et enregistrer ses modifications en base ?

## Sujets dépriorisés

- amélioration de l'export
- archivage


# 17 mai 2024 - Lancement sur les critères métiers

## Ordre du jour

- météo
- démo
- points sur les questions de la dernière revue
- questions/clarifications
- points sur les priorités
- prochaine revue - 3 juin à 11h

## Démo

![Matrice des critères métiers](images/240517/matrix.png)

## Questions de la dernière fois

- @Mauko donnera une suite à l'atelier
- @Christopher est en contact avec la PPSP (Pole de Production Service Proximité) pour explorer davantage ce sujet


## Questions à explorer

- [Éclaircir le critère consolidé "Toute demande par défaut"](https://github.com/dataoffice-ansm/gdr_sante/issues/247)
- [Déclaration d'accessibilité à explorer](https://github.com/dataoffice-ansm/gdr_sante/issues/238)
- [Affichage des messages d'erreurs](https://github.com/dataoffice-ansm/gdr_sante/issues/249)
- [Lenteur coté STD depuis leur maj](https://github.com/dataoffice-ansm/gdr_sante/issues/250) : réunion le 21 mai pour en connaitre la cause

## Points sur les priorités

![Tableau de bord](images/240517/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)

# 3 juin 2024 - Première version du calcul des critères métier

## Ordre du jour 

- Météo 
- Démo
- Retour sur les questions de la dernière revue
- Questions/clarifications
- Priorités

## Démo

![Image](images/240603/demo.png)

## Retour sur les questions de la dernière revue

- Identification des types de procédure : ne pas utiliser les id OTES
- [Déclaration d'accessibilité à explorer](https://github.com/dataoffice-ansm/gdr_sante/issues/238) -> En cours
- Point sur la [Lenteur coté STD depuis leur maj](https://github.com/dataoffice-ansm/gdr_sante/issues/250) -> La lenteur n'est pas due à GDRSanté

## Questions/clarifications

- Quel comportement en cas d'absence de donnée dans la matrice ?
    - Case quotation vide : Hypothèse : niveau maximal de risque, mais quel process ? 
    - Déclaration d'erreur au lancement ?
    - **Remonter une erreur comme quoi le fichier est corrompu**
    - **A terme inclure une validation à l'étpe de lancement du service (Dockerfile) si le fichier est corrompu**

## Questions/clarifications

- En cas de plusieurs procédures quel type de procédure utiliser ? 
    - Quel type de procédure choisir ? 
    - Doit on combiner les résultats ?
    - **Ce cas n'arrivera pas, l'analyse de risque s'applique uniquement aux demandes d'AMM de type "Demande intiale" pour lesquelles on aura toujours 
    une seule précédure**
- Suite de l'atelier métrique : En réflexion
- Passage à Paris le jeudi 27 juin

## Priorités

![Tableau de bord](images/240603/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)

## Agenda

- Réunion de travail avec la DA prévue le 12/06
- Réunion de COPIL prévue le 26/06

# Vendredi 14 juin - Appel à un expert et validation de la matrice
## Ordre du jour

- Météo
- Démo
- Retour sur les questions de la dernière revue
- Questions/clarifications
- Priorités

## Démo

![Expert](images/240614/demo.png)


## Retours sur la démo

La case "Risque" devrait plutôt s'appeler "Présent"
Les couleurs des experts sont dispos dans le figma composants

## Démo

![Matrice invalide](images/240614/demo2.png)

## Démo

![Matrice valide](images/240614/demo3.png)


## Retour sur les questions de la dernière revue

- Atelier métrique : Mise en suspens du sujet
- Retour sur la réunion de mercredi avec la DA : 
  - simplification des valeurs de cotation : que 0,1,2,3, à priori pas de  niveau 'A' et 'B' ou 'AB'
  - Récap des échanges avec STD : 
  1/ GDR : calculer l'analyse des risques
  2/ envoi à STD de l'analyse des risques
  3/ dans STD, récupreration des critères identifiés et les cotations, possibilité de changer la cotation
  - Réunion avec Talend à prévoir dans les 2 prochaines semaines : potentiellement le 27/06/2024

## Questions/clarifications

- Budget de la phase 2
- Mise à disposition des utilisateurs : 
  - Gestion de l'affichage des détails sur la page soumission
  - Besoin d'évolution des échanges avec STD pour permettre la mise à disposition des utilisateurs
- Question sur les board interdisciplinaire : on l'enlève


## Priorités

![Tableau de bord](images/240614/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)

## Agenda

- Réunion de COPIL prévue le 26/06
- Journée à Paris le 27
- Erica : absence pour trois jours de formation 
- Amélie : congés du 1er au 22 juillet

## Nouvelles issues à créer :

- Pouvoir mettre à jour la matrice sans passer par le DSI :
  - [BACK - Mettre à jour la nouvelle matrice dans l'API](https://github.com/dataoffice-ansm/gdr_sante/issues/282)
  - [FRONT: Créer une page pour màj matrice](https://github.com/dataoffice-ansm/gdr_sante/issues/283)

- Faire en sorte que la sélection des critères s'enregistre avant de cliquer sur "envoyer à STD" : ie sauvegarder un brouillon : enregistrer les infos au fur et à mesure des modifications à chaque clic
  - [Back DB - sauvegarde brouillon des données utilisées pour l'analyse metier](https://github.com/dataoffice-ansm/gdr_sante/issues/285)
  - [Back : Sauvegarde du brouillon de l'analyse des risques métiers](https://github.com/dataoffice-ansm/gdr_sante/issues/286)
  - [Back - Lecture des données sauvegardées pour l'analyse des risques métiers](https://github.com/dataoffice-ansm/gdr_sante/issues/287)
  - [Front : Affichage des données utilisées pour l'analyse métier](https://github.com/dataoffice-ansm/gdr_sante/issues/289) : en attente des retours de Thais sur la maquette

- Sauvegarder la "copie validée" lorsque l'on a cliqué sur "Envoyer à STD" :
  - [Back - Valider l'analyse des risques](https://github.com/dataoffice-ansm/gdr_sante/issues/288)


# Jeudi 27 juin - Journée à l'ANSM

## Ordre du jour 

- Météo 
- Démo
- Retour sur les questions de la dernière revue
- Questions/clarifications
- Planification de l'interruption en octobre
- Priorités

## Démo

![](images/240627/demo.png)

## Démo

![](images/240627/demo2.png)

## Retour sur les questions de la dernière revue

- Copil du 26 juin
  - Fix bug pour le calcul des critères métier : récupération de la bonne colonne
- Extract Codex avec code NL

## Questions/clarifications

- Recette page overview, quelle priorité pour : 
  - Travailler les marges des blocs ["accordeons"](https://github.com/dataoffice-ansm/gdr_sante/issues/293) dans le risque situationnel = pas prioritaire
  - Travailler sur le fait de rendre le comportement [responsive sur petit écran](https://github.com/dataoffice-ansm/gdr_sante/issues/294) = on laisse comme ça
- Roadmap : 
  - créer un formulaire pour permettre aux experts métier de contrôler des cotations inscrites dans le fichier entrant de la matrice 
  - Sauvegarde anonyme des évaluations
  - Réflexion sur la mise à disposition utilisateurs pour avoir un retour d'UX
- Juillet : période plus calme, temps de refactorisation

## Planification de l'interruption en octobre

- Rappel de l'estimation du budget qui ne sera pas consommé fin octobre : environ 29 000 €HT soit 36 jours travaillés
- Retour attendu d'ici le 12/07/2024 quant à l'évolution de GDR-Santé en 2025

## Priorités

![Tableau de bord](images/240627/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)

## Agenda

- Réunion de travail le 23/07/2024 : présentation de Grist comme éventuelle solution pour le remplissage de la matrice par les experts métier

## Nouvelles épiques identifiées
- [Remplissage des cotations de la matrice de critères métiers](https://github.com/dataoffice-ansm/gdr_sante/issues/309) : prioritaire
- Gestion des droits d'accès : [Identification des utilisateurs](https://github.com/dataoffice-ansm/gdr_sante/issues/234)
- [sauvegarde des choix des critères métiers et envoi à std](https://github.com/dataoffice-ansm/gdr_sante/issues/235)
- [automatisation des critères métiers](https://github.com/dataoffice-ansm/gdr_sante/issues/308)
- [Prise en compte de la recevabilité d'un dossier définie côté STD](https://github.com/dataoffice-ansm/gdr_sante/issues/311)
- [Stockage des fichiers](https://github.com/dataoffice-ansm/gdr_sante/issues/312)

## Nouvelles issues identifiées
- [Réfléchir à une interface Grist pour le remplissage des différents des critères métiers d ela matrice par les experts métiers](https://github.com/dataoffice-ansm/gdr_sante/issues/306)
- [Rajout de tests unitaires suite au bug fix de l'index des procedures types dans le fichier de la matrice](https://github.com/dataoffice-ansm/gdr_sante/issues/304)
- [Gestion des droits - NAM / SAML2](https://github.com/dataoffice-ansm/gdr_sante/issues/307)
- [[BACK]: création d'une route d'API pour interroger la recevabilité d'un métier auprès de STD](https://github.com/dataoffice-ansm/gdr_sante/issues/310)


# Vendredi 26 juillet - Stockage en base des critères métiers

## Ordre du jour 

- Météo 
- Démo
- Travaux en cours
- Questions / Points à clarifier
- Priorités


## Démo
- Ajout d'une documentation générale structurée
![](images/240726/demo3.png)


## Démo 
- Analyse des risques métiers : séparation du choix des critèrtes métiers et du résultat de l'analyse
![](images/240726/demo5.png)

## Démo 
- Analyse des risques métiers : séparation du choix des critèrtes métiers et du résultat de l'analyse
![](images/240726/demo6.png)

## Demo
- Ajout de la déclaration d'accessibilité
![](images/240726/demo4.png)

## Demo
- Ajout des critères métier dans la base de données
![](images/240726/demo7.png)

## Travaux en cours

- Proposition formulaire de remplissage des cotations inscrites dans le fichier entrant de la matrice
![](images/240726/demo1.png)
[Proposition 1](https://multicoop.getgrist.com/4cn1dn4Myi8j/AMMcriteria/p/12)


## Travaux en cours
- Proposition formulaire de remplissage des cotations inscrites dans le fichier entrant de la matrice
![](images/240726/demo2.png)
[Proposition 2 ](https://multicoop.getgrist.com/4cn1dn4Myi8j/AMMcriteria/p/18)


## Questions sur propositions interface Grist

- Ou heberger le tableau grist ? 
- Comment importer dans un autre GRIST
- Rendre le plus visible possible l'EXPERT concerne par la page
- Pour chaque page de chaque expert, précompléter les bases légales concernées par critères
- Afficher dans la page une section commentaire pour annoter les criteres

## Priorités

![Tableau de bord](images/240726/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)

## Agenda

- Réunion de travail sur la gestion des droits à caler le mardi 27/08/2024

## TODO (Prioriataire)

- Livrer une variante à utiliser sur le serveur de prod incluant : 
  - la correction du bug observée sur la v1.6.15 de main
  - [rollback de l'endpoint ̀`risk-overview/cis`](https://github.com/dataoffice-ansm/gdr_sante/issues/322)
  - [Suppression de la partie des risques métiers dans la page soumission](https://github.com/dataoffice-ansm/gdr_sante/issues/324)
- Fait et livré :  [variant/324-without_domain_criteria](https://github.com/dataoffice-ansm/gdr_sante/commits/variant/324_without_domain_criteria/)


## TODO

- Livrer en recette les dernières modifications apportées dans main incluant les mises à jour apportées par Talend incluant :
  - la correction du bug observée sur la v1.6.15 de main
  - [la prise en compte du rôle de la France dans les informations retournées par Talend](https://github.com/dataoffice-ansm/gdr_sante/issues/319)
  - [la prise en compte de la date de réception d'une soumission dans les informations retournées par Talend](https://github.com/dataoffice-ansm/gdr_sante/issues/323)
- Fait mais non mergé [feat/319-add-submission-data](https://github.com/dataoffice-ansm/gdr_sante/commits/feat/319_add_submission_data/) implique un breaking change

## TODO

- Continuer [travail en cours sur interface Grist](https://github.com/dataoffice-ansm/gdr_sante/issues/306)
- Fait, fichier grist fourni via Slack 02/08/2024

## TODO

- Issues rendues prioritaires :
  - [Gestion des droits](https://github.com/dataoffice-ansm/gdr_sante/issues/307)
  - [Récupération des fichiers données hébergés sur un S3](https://github.com/dataoffice-ansm/gdr_sante/issues/326)

## Autres issues créées

- [Bug fix affichage des accents circonflexes dans l'UI page soumission risque situationnel](https://github.com/dataoffice-ansm/gdr_sante/issues/327)

# Vendredi 9 août - Gestion des critéres métier via Grist 

## Ordre du jour 

- Météo 
- Démo
- Travaux en cours
- Questions / Points à clarifier
- Priorités

## Démo
- Proposition d'[interface Grist](https://multicoop.getgrist.com/gKug3VrRfVcF/AMMcriteria/p/9) pour le remplissage des données de la matrice des critères métiers
![](images/240809/demo.png)

## Démo
- Proposition d'interface Grist pour le remplissage des données de la matrice des critères métiers
![](images/240809/demo1.png)

## Travaux réalisés
- Amélioration de la doc
- Mise aux normes du fonctionnement poetry
- Rajout de cas de tests unitaires pour la lecture de la matrice des critères métiers

## Travaux en cours
- [Livrer en recette les mises à jour apportées par Talend incluant](https://github.com/dataoffice-ansm/gdr_sante/pull/329) :
  - Le rajout de la date de création d'une soumission
  - la prise en compte du rôle de la France dans les informations retournées par Talend
  - Ces travaux ont été réalisés sur une branche de travail mais pas encore mergé, NB: ils impliquent un breaking change

- [Récupération de fichiers hébergés sur MinIO](https://github.com/dataoffice-ansm/gdr_sante/issues/326)

## Questions / Points à clarifier

- Départ de Clément pour 3 semaines, on laisse donc en l'état pour que les jobs Talends puissent continuer à fonctionner, on mergera la branche [feat/319-add-submission-data](https://github.com/dataoffice-ansm/gdr_sante/commits/feat/319_add_submission_data/) au retour de Clément

- Le stockage des fichiers externalisé ne se fera pas sur un serveur MinIO. Une solution de stockage de fichier sur la GED ANY est en cours de réflexion : Réunion de travail entre Christopher et le gestionnaire de logciel ANY prévue le 19/08/2024 : Issue [Récupération des fichiers données hébergés sur un S3](https://github.com/dataoffice-ansm/gdr_sante/issues/326) clôturée

## Priorités

![Tableau de bord](images/240809/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)


## Agenda

- Réunion de travail sur la gestion des droits prévues le mardi 27/08/2024 à 10h


## TODO

- Poursuite de l'épique [sauvegarde des choix des critères métiers](https://github.com/dataoffice-ansm/gdr_sante/issues/235): 
  issue [Sauvegarde du brouillon de l'analyse des risques métiers](https://github.com/dataoffice-ansm/gdr_sante/issues/286)


# Vendredi 23 août - Sauvegarde des critères métiers

## Ordre du jour 

- Météo 
- Démo des travaux en cours
- Rappel des travaux en attente de mise en recette
- Questions / Points à clarifier
- Priorités

## Démo - travaux en cours
- [Sauvegarde des choix des critères métiers](https://github.com/dataoffice-ansm/gdr_sante/issues/235)
![](images/240823/demo1.png)

## Démo
![](images/240823/demo2.png)

## Démo
![](images/240823/demo3.png)

![](images/240823/demo4.png)

## Démo
![](images/240823/demo5.png)


## Rappel des travaux réalisés précédemment en attente de mise en recette
- [Prise en compte des mises à jour apportées par Talend](https://github.com/dataoffice-ansm/gdr_sante/pull/329) incluant :
  - Le rajout de la date de création d'une soumission
  - la prise en compte du rôle de la France dans les informations retournées par Talend
  - Branche de travail associée [feat/319-add-submission-data](https://github.com/dataoffice-ansm/gdr_sante/commits/feat/319_add_submission_data/) qui sera mergé au retour de Clément


## Questions 

- Stockage externalisé des fichiers : retour sur la réunion avec la gestionnaire de logiciel ANY
  - Le test de la solution de gestion de fichiers par le GED n'est pas concluante pour l'instant : On repart sur la solution d'un stockage externalisé sur une instance MinIO
  - Demi journée d'atelier à caler avec Christopher pour démonstration instance MinIO et voir pour utilisation d'une instance de test

- Interface Grist : suites à donner ? 
  - Une instance DINUM peut être utilisée pour permettre d'effectuer les tests auprès des experts qui remplissent les données de la matrice
  - Point à caler avec Thaïs à son retour de vacances

- Informations sur l'évolution de GDR en 2025 ? 
  - Pas d'info


## Autres sujets abordés

- Nouveaux jobs talend et nouvelle version d'entrants AMM à venir (plus de nouvelles dans 15 jours)

## Priorités

![Tableau de bord](images/240823/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)


## Agenda

- Réunion de travail sur la gestion des droits prévues le mardi 27/08/2024 à 10h


## TODO

- Poursuite de l'épique [sauvegarde des choix des critères métiers](https://github.com/dataoffice-ansm/gdr_sante/issues/235) :

  issue [[BACK] : Update des critères métiers sélectionnés sauvegardés en base de données #346](https://github.com/dataoffice-ansm/gdr_sante/issues/346)

  issue [[FRONT] : Passer au BACKEND (controller) l'information des critères métiers sélectionnés #339](https://github.com/dataoffice-ansm/gdr_sante/issues/339)

- Préparation de la réunion sur la [gestion des droits](https://github.com/dataoffice-ansm/gdr_sante/issues/307)

- Stockage externalisé des fichiers - [solution MinIO](https://github.com/dataoffice-ansm/gdr_sante/issues/326) : caler une date d'atelier avec Christopher

- Interface Grist : Demande de retours à Thaïs


# Vendredi 6 septembre Création des Endpoints pour STD

## Ordre du jour 

- Météo 
- Rappel des priorités en cours
- Démo
- Rappel des travaux en attente de mise en recette
- Questions techniques
- Questions administratives
- Mise à jour des priorités
- Agenda

## Rappel des sujets prioritaires

- [Gestion des droits](https://github.com/dataoffice-ansm/gdr_sante/issues/307)

- [Création d'un nouveau point d'API retournant les infos sommaires d'une soumission à STD comprenant](https://github.com/dataoffice-ansm/gdr_sante/issues/354) : 
   - Id de la soumission
   - Niveau de risque global
   - Liste des spécialités concernées avec pour chacune leur CIS et leur niveau de risque (label et valeur)

## Rappel des sujets prioritaires

- [Prise en compte de la recevabilité d'une soumission par GDR](https://github.com/dataoffice-ansm/gdr_sante/issues/310)

- [Renvoit de l'expertise d'une soumission à STD une fois que l'analyse des risque métiers est validée](https://github.com/dataoffice-ansm/gdr_sante/issues/288) : 
  - id de la soumission
  - liste des expertises (experts à solliciter + cotation maximale pour chaque expert)

## Démo - Gestion des droits

A compléter
![](images/240906/demo1.png)

## Démo - Création d'un nouveau point d'API GET pour une soumission

![](images/240906/demo2.png)


## Démo - Recevabilité d'une soumission

![](images/240906/demo3.png)

## Démo - Recevabilité d'une soumission

![](images/240906/demo4.png)

## Démo - Recevabilité d'une soumission

![](images/240906/demo5.png)

## Démo - Finalisation de l'analyse des risque métiers, format des infos envoyées à STD

![](images/240906/demo6.png)


## Rappel des travaux réalisés précédemment en attente de mise en recette
- [Prise en compte des mises à jour apportées par Talend](https://github.com/dataoffice-ansm/gdr_sante/pull/329) incluant :
  - Le rajout de la date de création d'une soumission
  - la prise en compte du rôle de la France dans les informations retournées par Talend
  - Branche de travail associée [feat/319-add-submission-data](https://github.com/dataoffice-ansm/gdr_sante/commits/feat/319_add_submission_data/) qui sera mergé au retour de Clément


## Questions techniques

- Envoi par TALEND de l'info de la base légale liée à une soumission ?

- Interface GRIST pour le remplissage des critères métiers : des retours utilisateurs ?


## Questions administratives

- GDR en 2025 : Mission de MCO avec ATOL CD 
  - La mission intègre-t-elle bien la pursuite du chantier GDR ?
  - Quel est le budget total alloué pour cette mission ?
  - Y-a-t-il une estimation de la décomposition du budget entre ATOL CD pour la maintenance et multi pour le développement ?
  - Date de début souhaitée ?

- Erica est passée à 90% début juin 2024


## Mise à jour des priorités

![Tableau de bord](images/240906/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)

## Agenda

- Réunion de travail / restitution sur la gestion des droits prévue le mardi 24/09/2024
- Pochain COPIL le 25/09/2024

## TODO

- [Liste des critères avec noms à rajouter dans les info submission finalisée evoyée à STD](https://github.com/dataoffice-ansm/gdr_sante/issues/365)

- Gestion des droits formulaire Grist : caler Réunion de travail avec Thais

## Actualisation des priorités

- Planning prévisionnel ANSM
![planning prévisionnel](images/240906/planning_previsionnel_ANSM.png)

## Actualisation des priorités

- [Gestion des droits](https://github.com/dataoffice-ansm/gdr_sante/issues/307) :
  - version à livrer la semaine d'avant le 24/09 (car migration sur les serveurs de l'ANSM prévue avant le 24) : nécessaire d'avoir l'info nom, prenom, direction, pole en un seul token crypté ## Questions 

## Actualisation des priorités

- Finaliser la [sauvegarde en base de données des critères métiers et envoi des expertises à STD](https://github.com/dataoffice-ansm/gdr_sante/issues/235) : 
  - finaliser de la partie [FRONT pour la sauvegarde en base de données de l'analyse des critères métier](https://github.com/dataoffice-ansm/gdr_sante/issues/352)
  - [Prise en compte de la base légale](https://github.com/dataoffice-ansm/gdr_sante/issues/359) : en attente de la transmission des infos entrantes de submssion par EntrantsAMM
  
- Stockage externalisé des fichiers - [solution MinIO](https://github.com/dataoffice-ansm/gdr_sante/issues/326)  : dépriorisé


# Vendredi 20 septembre 
__Gestion des droits Keycloak et API STD__

## Ordre du jour 

- Météo 
- Rappel des sujets prioritaires
- Démo
- Rappel des travaux en attente de mise en recette
- Questions 
- Mise à jour des priorités
- Agenda

## Rappel des sujets prioritaires

- [Gestion des droits](https://github.com/dataoffice-ansm/gdr_sante/issues/307) :
  - version à livrer la semaine d'avant le 24/09 

- [Modifier le format de l'expertise d'une submission renvoyée à STD une fois que l'analyse des risques métier est validée](https://github.com/dataoffice-ansm/gdr_sante/issues/365) : 
  - rajout de la liste des critères concernés

- [Modifier le format de données entrantes pour un POST de submission](https://github.com/dataoffice-ansm/gdr_sante/issues/373) : 
  - Modification à apporter suite à réunion du 13/09/2024 avec Clément

- [Finalisation de l'analyse du risque métier](https://github.com/dataoffice-ansm/gdr_sante/issues/352) : 
   - sauvegarde de l'analyse du risque métier en base de données
   - envoi des données à STD
   - Travaux en cours sur [branche de travail feat/352](https://github.com/dataoffice-ansm/gdr_sante/tree/feat/352-FRONT-page-soumission-finalisation-analyse-du-risque-m%C3%A9tier-d%C3%A9finitive)


## Démo travaux en cours - sauvegarde de l'analyse du risque métier et envoi à STD
![](images/240920/demo1.png)

## Démo travaux en cours - sauvegarde de l'analyse du risque métier et envoi à STD
![](images/240920/demo1a.png)

## Démo - Modification du format de l'expertise d'une soumission

![](images/240920/demo2.png)

## Démo - Gestion des droits

![](images/240920/demo3.png)

## Démo - Gestion des droits

![](images/240920/demo3a.png)

## Rappel des travaux réalisés précédemment en attente de mise en recette
- [Prise en compte des mises à jour apportées par Talend](https://github.com/dataoffice-ansm/gdr_sante/pull/329) incluant :
  - Le rajout de la date de création d'une soumission
  - la prise en compte du rôle de la France dans les informations retournées par Talend
  - Branche de travail associée [feat/319-add-submission-data](https://github.com/dataoffice-ansm/gdr_sante/commits/feat/319_add_submission_data/) 

- Il a été décidé en réunion du 13/09/2024 que cette branche de travail ne serait finalement pas mergée et que l'on utilisera directement les infos d'Entrants AMM

## Questions 

- 

## Agenda

- Réunion de travail / restitution sur la gestion des droits prévue le mardi 24/09/2024
- Pochain COPIL le 25/09/2024

## Mise à jour des priorités

![Tableau de bord](images/240920/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)


# Vendredi 4 octobre
__STD, EntrantAMM et Docker__ 

## Ordre du jour 

- Météo 
- Rappel des sujets prioritaires
- Démo
- Questions 
- Mise à jour des priorités
- Agenda


## Rappel des sujets prioritaires

- [Gestion des droits](https://github.com/dataoffice-ansm/gdr_sante/issues/307)

- [Deploiement sur serveur interne](https://github.com/dataoffice-ansm/gdr_sante/issues/273)

- [Prise en compte des données des dossiers issues d'Entrant AMM alimentées par Talend](https://github.com/dataoffice-ansm/gdr_sante/issues/373)

- [Finalisation de l'analyse du risque métier](https://github.com/dataoffice-ansm/gdr_sante/issues/352)


## Démo - Envoi des données à STD
![](images/241004/demo1.png)

## Démo - Envoi des données à STD
![](images/241004/demo2.png)

## Démo - Envoi des données à STD
![](images/241004/demo3.png)

## Démo - Envoi des données à STD
![](images/241004/demo4.png)

## Démo - Envoi des données à STD
![](images/241004/demo5.png)

## Démo - Prise en compte des données d'une soumission provenant d'EntrantAMM
![](images/241004/demo6.png)

## Démo - Prise en compte des données d'une soumission provenant d'EntrantAMM
![](images/241004/demo7.png)

## Démo - Nouveau Docker ! 

:::::::::::::: {.columns}
::: {.column width="50%"}
Uniformisation des environnements : 

- dev
- ci
- prod
::: 

::: {.column width="50%"}
![](images/241004/demo8.png)
:::
::::::::::::::


## Questions 
- Quel est le point d'API d'STD à utiliser pour envoyer les données d'une soumission dont l'analyse a été validée ?

- Si une personne n'est pas connectée, a-t-elle le droit de visualiser si l'analyse a déjà été envoyée à STD ou pas ? 

## Questions - Ex :  non connectée
![](images/241004/Question1.png)

## Questions - Ex : connectée
![](images/241004/Question2.png)


## Agenda

- Retour sur le dernier COPIL du 25/09/2024

## Mise à jour des priorités

![Tableau de bord](images/241004/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)


## Notes diverses

- Envoyer à Christopher la listes des énumrations des types et sous-types de sousmisison pouvant provenir d'Entrant AMM
- Côté Entrant AMM, la base légale est inscrite sous forme de booléen pour chaque article. Ceci étant selon Thaïs, l'intitulé de la base légale est visble textuellement dnas l'interface. C'est donc possible de leur côté de récupérer une énumération des intitulés de bases légale provenant d'ENtrant AMM : Christopher nous confirmera cete info.
- Créer une issue pour la mise en place un fichier de configuration externe pour lister l'intitulé des bases légales possible -> pour la suite de GDR en 2025
- La date de réception d'un dossier n'est pas exploitée dans l'UI : cf [issue](https://github.com/dataoffice-ansm/gdr_sante/issues/396)
- L'envoi des données à STD d'une soumission validée se fera via un point d'API inscrit en varable d'environnement
- Nécéssité d'être authentifié pour requeter l'API d'STD -> url d'authentification à requeter pour récupérer le token préalablement à la requête : c'est le même que celui utilisé pour l'authentification 
- L'API d'STD sera probablement pas dispo avant fin octobre
- Test du déploiement avec la nouvelle version de Docker prévue avec Christopher lundi 07/10
- Test de l'authentification prévue le mardi 15/10 à 14H

- Retour sur la question 2 : 
Levée d'ambiguité entre "connexion" et droit de modification
  -> si pas connecté : pas d'accès à GDR du tout
  -> si role en lecture seule : possibilité de voir si la submission a été transmise à STD

- Retour COPIL :
Surtout retour sur GRIST

- Retour du CASAR
Date de mise à jour des risques situationnels disponible ? Créer un ticket pour rajouter la date dans un fichier de conf -> pour la suite de GDR en 2025

- Chantier Minio : redevient prioritaire pour les fichiers qui viennent de CODEX : fichiers code CIS et code ATC

## Mise à jour des priotités

- [NAM](https://github.com/dataoffice-ansm/gdr_sante/issues/379)

- [Fichier GRIST](https://github.com/dataoffice-ansm/gdr_sante/issues/389) : partir sur l'export des données sources "évaluation"

- [Base légale : liste des intitulés de bases légales provenant d'ENtrantAMM à modifier sur la base des infos d'ATOL](https://github.com/dataoffice-ansm/gdr_sante/issues/393)

- [Chantier Minio](https://github.com/dataoffice-ansm/gdr_sante/issues/326) : devient prioritaire pour les fichiers qui viennent de CODEX : fichiers code CIeS et code ATC

- [Appel à l'API STD pour envoi des données de soumission dont l'anyalse des risques métier est validée](https://github.com/dataoffice-ansm/gdr_sante/issues/383)

- [Prendre en compte des retours de Thaïs sur l'UI](https://github.com/orgs/dataoffice-ansm/projects/1/views/1?pane=issue&itemId=82172661)

- [Rajouter la date de réception d'un dossier dans le bloc information d'une soumission dnas l'UI](https://github.com/dataoffice-ansm/gdr_sante/issues/396)

# Vendredi 18 octobre
__Authentication, Doc et accès aux fichiers__ 

## Ordre du jour 

- Météo 
- Démo
- Hors code 
- Mise à jour des priorités
- Agenda

## Démo - GDR en prod !
![](images/241018/auth.png)

## Démo - Prise en charge des scopes
![](images/241018/scope.png)

## Démo - Documentation
![](images/241018/doc.png)

## Hors code 

- Poursuite de la recherche d'un vecteur commercial pour GDR 2025
- Quelques jours de plus sur novembre/décembre (a priori 5 à valider au retour d'Amélie)

## Mise à jour des priorités

![Tableau de bord](images/241004/board.png)
[github](https://github.com/orgs/dataoffice-ansm/projects/1/views/1)

## Agenda

- Visite le 31 octobre, costume ou pas ?
- mise en service mi-janvier


## Note :
- logger les informations du NAM dans le header (apres le nom) -> livré 3.3.0
- limiter accès à la MAJ pour les membres de la (DA)
- deposer une archive avec api.json + 2 images docker  -> livré 3.4.0
-  ["NAT", "MRP", "DCP", "MRP_DCP"] 


# Vendredi 31 octobre
__Critère métier Grist__

## Ordre du jour 

- Météo 
- Démo
- Mise à jour des priorités
- Hors code 
- Agenda

## Démo 
![](images/241031/grist.png)

## Mise à jour des priorités 

![](images/241031/board.png)

## Hors Code
- Piste INOPS 
  - markup INOPS + markup UGAP (Stéphane)
- Stephane - 2 autres pistes

## Agenda 

## Notes 
- livrer la nouvelle version du DomainCriteria Dao
- livraison des encours et a faire


# Lundi 10 février : __Lancement 2025__

## Ordre du jour 

- Météo 
- Mise à jour des priorités
- Questions
- Agenda

## Mise à jour des priorités 

![](images/250210/board.png)

## Questions
- [Issue 431](https://github.com/dataoffice-ansm/gdr_sante/issues/431) : est-ce que s'il n'y a aucune évaluations un agent peut tout de même cliquer sur "Valider et envoyer à STD" -> Oui c'est posisble

- [Issue 431](https://github.com/dataoffice-ansm/gdr_sante/issues/431) : Modification d'un même dossier pour 2 utilisateurs : précision sur l'observation faite 
-> Dans la théorie un seul agent est en charge à un seul dossier.
-> Le problème a résoudre est plus lié à l'étape intermédiraire de validation des critères, si une autre personne revient dessue ensuite --> C'est ce qui a va être résolu par la modal demandé par Thaïs.

- [Issue 396](https://github.com/dataoffice-ansm/gdr_sante/issues/396) : Affichage de la date de réception d'une soumission toujours nécessaire ? Quelle priorité -> Prioritaire à faire avant les tests

## Agenda 

- prochaine réunion le 24/02/2025 à 14h
- Démarrage des tests : 3 semaines  à partir du 10/03/2025
- prochain COPIL le 12/02/2025, le 12/03/2025, le 09/04/2025
- Départ Erica : 24/03/2025

## Compte Rendu : 

Pour les tests prévus d'être commencés le 10/03, priorités des choses à faire :
- Epic 431
- Epic 446 Informations sur les submissions

Epic 311 Echanges entre STD et GDR : non prioritaire pour ce sprint, Christopher n'est pas là et pas de retour de STD

Priorité à venir post sprint :
- automatisation des critères
- pouvoir changer la source des fichiers, récupération des dnnées par API (epic 312) horizon 6 mois :

Issue #212 : ne devient plus prioritaire -> passé en dépriorisé
Issue #394 : ne devient plus prioritaire -> passé en dépriorisé

Issue 353 et 449 (gestion des critères métiers qui ont été supprimés entre temps) : 
- faire en sorte que le critère ne soit pas envoyé à STD
- prévenir l'utilisateur que le critère n'existe plus en grisé -> Thais pourra faire une maquette

Passage en revues des épiques 2025 : 
- #308 : limites à définir entre responsabilités de Talend et GDR, enjeu : pouvoir facilement paramétrer les nouveaux critères :  attendre le retour de Chrsitopher
- #448 : attendre le retour de Chrsitopher, ex Talend peut peut-être faire de la comparaison simple sur certains critères
- #182 (archivage des niveaux de risque): dépriorisé pour l'instant
- #150 : Très pratique, mais pour l'instant c'est difficile de définir si c'est un vrai besoin : le cas d'usage va être l'agent qui accède à un dossier qui y accède via STD. L'autre cas d'usage c'est quelqu'un qui veut accèder à un dossier. Potentiellement on peut passer par EntrantAMM et il y aura un lien vers  le dossier.-> En attente des résultats des tests


# Lundi 24 février 

## Ordre du jour 

- Météo 
- Travaux réalisés
- Démo
- Questions
- Mise à jour des priorités
- Agenda


## Travaux réalisés

- Mise à jour des dépendances python et node (passage de Next 14 à Next 15)
- Configuration d'un nouveau fournisseur OIDC
- En cours : amélioration de l'UI

## Démo : Amélioration de l'UI

![](images/250224/demo1.png)

## Démo : Amélioration de l'UI

![](images/250224/demo2.png)

## Démo : Amélioration de l'UI

![](images/250224/demo3.png)

## Questions
- Retour sur le COPIL du 12/02/2025 -> RAS
- Affichage de la date : timestamp en seconde a vérifier avec Tallend -> oui c'est bien ça
- Utilisateur admin keycloak pour Christopher/Thais -> a voir après les tests utilsateurs (si ça trouve le NAM ça sufft)
- Quel affichage du niveau de risque pour les données de quotation manquantes ? -> Thais nous enverra la maquette pour ça pour avoir un message qui dit que comme l'info était manquante on a mis le niveau max à 3
  Quelle échéance ? -> remplir au niveau max dans un premier temps pour le 10/03 et dnas un deuxième temps inscrire un message dans l'UI pour préciser à l'utilisateur que des valeurs 


## Mise à jour des priorités 

![](images/250224/board.png)

## Agenda 
- Démarrage des tests : 3 semaines  à partir du 10/03/2025
- Prochains COPIL le 12/03/2025, le 09/04/2025
- Départ Erica : 24/03/2025
- Point d'avancement le lundi 03/03/2025

## CR

- Christpher : besoin de l'affichage du numéro CESP affichage pour accéder à des dossiers de façon simple.
 -> numéro pas encore reçu côté GDR 
 -> à terme ça pourrait être de récupérer le numéro CESP et le mettre dans l'url d'une soumission
 Question : quelle échéance ? (Après le 10 mars)

- STD est opérationnel => dans les tests utilsateurs on doit pouvoir intégrer la partie STD dans les tests utilisateurs pour le 10 mars, Christopher nous evnverra le token correspondant par slack

A vérifier :  
- est-ce que les critères sélectionnables sont uniquement des critères dont le niveau de risque est supérieur ou égal à 1
- cas de aucune évaluation à demander (pas de critères sélectionnés) : cf issue #474

Retour sur les questions :
- envoyer la procédure pour lancer la doc pour la configration de Keycloak à Christopher -> fait en direct

- Christopher : 2 fichiers liste code CIS - substances actives est-ce qu'on pourrait interroger CODEX pour avoir la liste des substances actives par API plutôt que passer par le fichier qui est chargé en amont ? -> cf Issue créée par Erica #480



# Lundi 3 mars

- Question : est-ce qu'on veut faire apparaître dans la sélection des critères les critères qui n'ont pas de niveau de risque > ou égal à 1 (0, NA, NC)