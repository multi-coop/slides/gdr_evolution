---
title: Revues d'itération GDR-Santé
subtitle: 
author: multi.coop
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo_client.svg"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# 26 avril 2024 - Soumission de la Page Dossier !

## Ordre du jour 

- météo 
- démo
- points sur les priorités identifiées
- questions/clarifications

## Démo

## Clarifications 
:::incremental

- Turn off connected [alarm](www.google.fr)
- Get out of bed
- One bullet after the other.

:::

---

## Getting breakfast

- Eat **eggs**
- Drink highlighted **coffee**, with **some more highlighted stuff**.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis 
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
eu fugiat nulla pariatur.

# In the evening

## Dinner

- Eat spaghetti
- Drink wine

# Lvl1 headers do not need to be on a separate slide

:::::::::::::: {.columns}
::: {.column width="50%"}
Some stuff on the left column
::: 

::: {.column width="50%"}
![Picture of spaghetti on the right](images/spaghetti.jpeg)
:::
::::::::::::::

## Going to sleep

- Get in bed
- Count sheep

---

- `---` as a slide separator when no title

```python
def some_code():
  print("Hello world !")
  pass
```
